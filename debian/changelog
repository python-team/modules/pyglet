pyglet (2.0.17+ds-3) unstable; urgency=medium

  * Team upload.
  * Adjust time-sensitive test exclusions for upstream changes in 1.4.2
    (closes: #1092867).

 -- Colin Watson <cjwatson@debian.org>  Mon, 20 Jan 2025 00:13:46 +0000

pyglet (2.0.17+ds-2) unstable; urgency=medium

  * Team upload.
  * Downgrade Recommends for older FFmpeg to Suggests

 -- Timo Röhling <roehling@debian.org>  Sun, 15 Sep 2024 16:42:06 +0200

pyglet (2.0.17+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.0.17+ds
  * FFmpeg is optional now

 -- Timo Röhling <roehling@debian.org>  Sun, 15 Sep 2024 15:10:33 +0200

pyglet (2.0.15+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.0.15+ds
  * Bump Standards-Version to 4.7.0
  * Migrate to PEP 517 build

 -- Timo Röhling <roehling@debian.org>  Thu, 11 Apr 2024 12:11:36 +0200

pyglet (2.0.10+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.0.10+ds
  * Drop obsolete Python 3.11 compat patch

 -- Timo Röhling <roehling@debian.org>  Mon, 13 Nov 2023 00:54:51 +0100

pyglet (2.0.9+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.0.9+ds
  * Update d/copyright
  * Refresh patches
  * Bump dependencies for ffmpeg 6 (Closes: #1042408)
  * Simplify d/rules and use pybuild-autopkgtest
  * Stop build-depending on obsolete packages
  * Bump Standards-Version to 4.6.2

 -- Timo Röhling <roehling@debian.org>  Sat, 29 Jul 2023 23:48:56 +0200

pyglet (1.5.27+ds-2) unstable; urgency=medium

  * Team upload.
  * Skip test that breaks with Python 3.11 (Closes: #1026690)

 -- Timo Röhling <roehling@debian.org>  Wed, 21 Dec 2022 23:02:05 +0100

pyglet (1.5.27+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.5.27+ds
  * Drop dependency on libgdk-pixbuf-2.0-0 (Closes: #967714)

 -- Timo Röhling <roehling@debian.org>  Thu, 13 Oct 2022 22:10:05 +0200

pyglet (1.5.26+ds-2) unstable; urgency=medium

  * Team upload.
  * Manually update library dependencies (Closes: #1004587)

 -- Timo Röhling <roehling@debian.org>  Wed, 06 Jul 2022 14:24:31 +0200

pyglet (1.5.26+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.5.26+ds
    - Fix build error with ffmpeg 5 (Closes: #1004587)
  * Bump Standards-Version to 4.6.1

 -- Timo Röhling <roehling@debian.org>  Fri, 01 Jul 2022 19:08:06 +0200

pyglet (1.5.14-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Replace libgdk-pixbuf2.0-0 dependency.
    (Closes: #992262)
  * debian/control: Bump Standards-Version to 4.6.0.

 -- Boyuan Yang <byang@debian.org>  Wed, 17 Nov 2021 09:12:25 -0500

pyglet (1.5.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Document ignoring interactive tests
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 29 Jan 2021 13:29:13 +0100

pyglet (1.4.10-2) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael R. Crusoe ]
  * debian/rules: better skip of the interactive tests
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * No tab in license text (routine-update)
  * debian/docs: don't ship the nonexistant docs directory

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 29 Jan 2021 09:49:46 +0100

pyglet (1.4.10-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * New upstream release
    - ensure compatibility with python 3.8; Closes: #950039
  * debian/watch
    - update to detect even latest upstream releases
  * debian/copyright
    - add proper upstream copyright notice
  * debian/patches
    - drop, refresh, update patches
  * Drop python2 support; Closes: #937445
  * debian/rules
    - exclude also test_player_silent_audio_driver tests
  * debian/docs
    - install README.md, renamed from README

 -- Sandro Tosi <morph@debian.org>  Mon, 03 Feb 2020 01:55:30 -0500

pyglet (1.4.1-3) unstable; urgency=medium

  * Belatedly acknowledge 1.3.0-2.1 NMU from Reinhard Tartler.  Disable
    ClockTimingTestCase for build reproducibility.  Closes: #929697.
  * Stop shipping extlibs/ in the binary packages and depend on the
    png and future modules instead.
  * Update library dependencies based on load_library calls.
  * Bump policy version to 4.4.0; no changes needed.

 -- Mark Hymers <mhy@debian.org>  Thu, 29 Aug 2019 20:54:18 +0100

pyglet (1.4.1-2) unstable; urgency=medium

  * Fix FTBFS by adding python-pytest and python3-pytest Build-Deps.

 -- Mark Hymers <mhy@debian.org>  Thu, 29 Aug 2019 01:31:53 +0100

pyglet (1.4.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/changelog: Remove trailing whitespaces.

  [ Mark Hymers ]
  * Update to upstream version 1.4.1
  * Drop patch "up_no_string_exceptions" as merged upstream
  * Add Recommends on libpulse0 for optional use in lib_pulseaudio.py.
  * Add 0001-Add-missing-import.patch and 0002-Handle-renaming-of-function.patch
    to fix test suite failures.
  * Re-instate running of font tests.
  * Update clean target to remove test cache.
  * Fix copyright file syntax error.
  * Add Build-Dependency on libpulse0.

 -- Mark Hymers <mhy@debian.org>  Mon, 26 Aug 2019 18:34:50 +0100

pyglet (1.3.0-2.1) unstable; urgency=medium

  [ Yaroslav Halchenko ]
  * No strings exceptions is allowed in Python2.6

  [ Reinhard Tartler ]
  * Non-Maintainer upload.
  * Disable ClockTimingTestCase (Closes: #929697)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 02 Jun 2019 15:17:34 -0400

pyglet (1.3.0-2) unstable; urgency=medium

  [ Scott Kitterman ]
  * Team upload
  * Drop obsolete provides and depends
  * Enhance package long descriptions
  * Update debian/copyright

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Adam Cecile ]
  * Port from CDBS to DH.
  * Enable integration unit tests.
  * Provide Python 3 packages (Closes: #822766).

 -- Scott Kitterman <scott@kitterman.com>  Fri, 01 Mar 2019 22:45:26 -0500

pyglet (1.3.0-1) unstable; urgency=medium

  * The released version

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 04 Jan 2018 17:15:56 -0500

pyglet (1.3.0~rc1-2) unstable; urgency=medium

  * Petter Reinholdtsen added python-future to Depends
    (Closes: #878011)

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 16 Oct 2017 17:49:18 -0400

pyglet (1.3.0~rc1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Yaroslav Halchenko ]
  * Fresh upstream rc (Closes: #786897, #871487)
    - resolves some of the segfaulting issues (Closes: #864936)
  * d/watch - point to pypi
  * d/patch - only 1 left not upstreamed (minor - string exception, PR sent)
  * d/rules + changelog -- no .dfsg suffix any longer.  It seems that all the
    materials previously removed carry statement of copyright/license of the
    main project
  * d/control - boosted policy to 3.9.8, removed obsolete min Python version
    specification
  * d/control,rules - run at least some of the upstream unittests

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 23 Aug 2017 01:30:59 -0400

pyglet (1.1.4.dfsg-3) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Remove DM-Upload-Allowed; it's no longer used by the archive software.

  [ SVN-Git Migration ]
  * git-dpm config.
  * Update Vcs fields for git migration.

  [ Mattia Rizzolo ]
  * Remove long useless debian/pycompat file.
  * Build using dh-python instead of python-support.  Closes: #786116
  * debian/control: bump standards-version to 3.9.6, no changes needed.
  * debian/copyright:
    + update first paragraph according to copyright-format 1.0.
    + fix missing-license-paragraph-in-dep5-copyright.
  * Bump debian/compat to 9.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 14 Dec 2015 02:18:12 +0000

pyglet (1.1.4.dfsg-2) unstable; urgency=low

  * Rebuild for upload to unstable

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 03 May 2011 18:24:43 -0400

pyglet (1.1.4.dfsg-1) experimental; urgency=low

  * New upstream release (Closes: #532807)
  * debian/copyright:
    - converted to DEP5 format
    - added myself to holders of debian/*
  * debian/control:
    - boosted standards version to 3.9.1.  BSD license included
      verbatim in a debian/copyright now
    - added myself to uploaders
  * debian/rules: get-orig-sources
    - not removing materials with included now copyright, free license
      terms
    - not pruning tools since script is minimal, written by the pyglet
      original author, part of the project, thus is covered by project's
      copyright/license
  * debian/source/format: switch source format to 3.0 (quilt)
  * Introduced patches:
    - up_no_string_exceptions: deduced two types of exceptions appropritate
      for the context to replace deprecated in 2.6 string exceptions
      (Closes: #585316)
    - up_typo_have_getprocaddress: minor typo
    - up_no_standard_config_available: use find_library first -- workaround
      the issue for NVidia GLX libraries (Closes: #584548)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 25 Dec 2010 22:25:44 -0500

pyglet (1.1.2.dfsg-3) UNRELEASED; urgency=low

  * debian/control
    - removed Ondrej from Uploaders: thanks for your work!

 -- Sandro Tosi <morph@debian.org>  Fri, 09 Oct 2009 22:56:40 +0200

pyglet (1.1.2.dfsg-2) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Stephan Peijnik ]
  * debian/watch
    - Fixed version mangling.
  * debian/control
    - Fixed Recommends for libopenal1 (was: libopenal0a) as reported
      by debcheck.
  * debian/copyright
    - Replaced (C) with copyright sign for Debian packaging work notice.

 -- Stephan Peijnik <debian@sp.or.at>  Thu, 19 Feb 2009 07:34:06 +0100

pyglet (1.1.2.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/rules: get-orig-sources now also removes the documentation
    due to unclean licensing in pyglet-1.1.2.
  * Added PYGLET_VERSION variable in debian/rules to simplify
    updating of get-orig-sources.

 -- Stephan Peijnik <debian@sp.or.at>  Fri, 31 Oct 2008 00:29:13 +0100

pyglet (1.1.1.dfsg.2-1) unstable; urgency=low

  * Removed examples broken by the removal of non-free files as per the
  upstream maintainer's request.
  * Updated debian/copyright:
    - Removed notices for files not in upstream tarball anymore.
    - Added myself to copyright holders of Debian packaging.
  * Updated debian/rules:
    - Repackaging as pyglet-1.1.1.dfsg.2.

 -- Stephan Peijnik <debian@sp.or.at>  Tue, 16 Sep 2008 11:48:42 +0200

pyglet (1.1.1.dfsg-1) unstable; urgency=low

  [ Stephan Peijnik ]
  * New upstream release (Closes: #494367).
  * Updated debian/control:
    - Added myself as uploader.
    - Updated Standards-Version.
  * Updated debian/rules: Updated get-orig-sources to remove non-free examples.
  * Updated debian/TODO:
    - Removed note about documentation
    - Added information about non-free files being removed.
  * Updated debian/watch: Added dversionmangle option.

 -- Ondrej Certik <ondrej@certik.cz>  Sun, 14 Sep 2008 23:56:18 +0200

pyglet (1.0.dfsg-1) unstable; urgency=low

  [ Michael Hanke ]
  * Initial release (Closes: #459729).
  * The whole debian packaging, except the changes described below:

  [ Ondrej Certik ]
  * debian/watch fixed to work with the latest pyglet release (1.0)
  * maintainer changed to Debian Python Modules Team
  * Vcs-Browser and Vcs-Svn fields changed to the DPMT svn repository
  * Ondrej Certik added to uploaders
  * DM-Upload-Allowed: yes field added
  * get-orig-sources rule added to debian/rules to create the dfsg.orig.tar.gz

 -- Ondrej Certik <ondrej@certik.cz>  Sat, 16 Feb 2008 13:13:35 +0100
